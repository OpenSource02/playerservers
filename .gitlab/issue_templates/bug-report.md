# Core informations
* About (PlayerServers / PSHubCore / PSServerCore) : 
* Version :
* BungeeCord (or what fork) :
* Spigot :

# About

**Issue**
In this segment, please describe us the issue you're having with our plugin, and possibly include screenshots and error logs

**How to reproduce?**
Please describe us how to reproduce the issue. It would help us a lot!

**Expected behavior**
A clear and concise description of what you expected to happen. If applicable, add screenshots or a recording to help explain your problem.