# PlayerServers

An advanced plugin which allows your players to have their own sub-server, install plugin & more

PlayerServers is an advanced plugin that will allow your players to create their own BungeeCord sub-server with just one command. On that server, they'll be owners and they'll have the whole control under that server. They'll even be able to install plugins on it using an advanced GUI 

# Features
- **ASYNC, LAG FREE**
The creation of servers happens 95% in async mode, which means that the creation of servers will be processed in a separate thread and 0 lag will be generated during that period.

- **SERVER CONTROL GUI**
Server owners will have their GUI control panel, that they'll be able to access with command /menu. It will allow them to manage various parts of the server (whitelist, bans, weather & more), give them an option to install, enable, or disable additional plugins and edit config files.

- **HubCore** - [CLICK HERE TO DOWNLOAD](https://www.spigotmc.org/resources/%E3%80%8C-playerservers-%E3%80%8Dhub-core-addon-for-playerservers.79144/)
When purchasing this plugin, you'll get an additional HubCore plugin which will allow you to make a compass that will show online & offline player servers and give your players the ability to connect to them. Furthermore, it will give you some basic options for your lobby like disabling rain, hunger & damage. A special placeholder that will allow players to see current player-count on their sub server is also included.

- **Fast & Efficient**
The creation of the server happens in less than 2  seconds which will allow your players to be teleported to their sub server in less than 15 seconds (other time will be used for boot-up of spigot server, and generating the main world - also, the huge factor is your server CPU)

- **Permissions based**
Everything is configurable with permissions. You can edit the amount of RAM a specific group has, max plugins, and max players per server.

- **NEW! PlayerServers support multiple host machines now!**
From the beginning of this project, the feature you've requested us to implement the most is the support for multiple host machines! We've worked for months in order to make it possible, but now, it's finally released! You can host your servers on multiple nodes and finely balance them across your infrastructure in order to flexibly scale RAM usage, CPU, and DISK.

- **Automatic & works out of the box**
The plugin is working out of the box without any need for external dependencies. Thanks to the integration of the Bungee Server Manager plugin (which I'm allowed to integrate, as it could be seen here), we were able to achieve the effect of automatically adding newly created sub servers to the BungeeCord config & server list without the need for reboot or greload. The same goes for the deletion of sub servers as well.

- **Simple Configuration**
The plugin has a simple configuration file where you'll be able to edit the max running instances count, per-server-ram, messages and more.

- **Automatic Server Shutdown**
Servers with 0 players will automatically shutdown if the time of the last player joining is longer than the configured one.
